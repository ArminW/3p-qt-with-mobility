#!/bin/bash
# @file install_windows.sh
# @brief On-the-fly replacement for the missing Qt install target handling on Windows
# use: install_windows.sh <qt source root> <install target root>

# @cond
# Copyright (C) 2012 arminweatherwax@lavabit.com 
# Thanks for an eye JustinCC!
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
# @endcond

export QT_BUILD="$1"
export INSTALL_DIR="$2"
export HERE=$(pwd)
export INCLUDE_DEST="$INSTALL_DIR/include"
export QT_SRC="$QT_BUILD/src"

mkdir -p "$INSTALL_DIR/lib/release"
cp -f "$QT_BUILD/lib/"*.dll "$INSTALL_DIR/lib/release"
cp -f "$QT_BUILD/lib/"*.lib "$INSTALL_DIR/lib/release"

for d in bin include mkspecs plugins translations; do
	cp -rf "$QT_BUILD/$d/" "$INSTALL_DIR/"
done


echo "#!/bin/bash" > $HERE/relocate.sh

pushd "$INCLUDE_DEST"

grep -ro "\.\.\/src.*" * | \
     perl -pi -e 's/\"//g'|  \
     perl -pi -e "\$baz=quotemeta('$QT_SRC');s/\.\.\/src/cp \-f \$baz/g" | \
     perl -pi -e "\$bar=quotemeta('$INCLUDE_DEST');s/^/\$bar\//g"| \
     perl -pi -e '/(.*):(.*)/ && print "$2 $1#";' |\
     perl -pi -e 's/#.*$//g'|  \
     perl -pi -e 's/\\//g' | \
      cat  >> $HERE/relocate.sh
popd

# chmod +x relocate.sh
# ./relocate.sh
# rm -f relocate.sh

