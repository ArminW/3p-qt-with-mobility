Currently the Windows build needs some things to be done by hand:

* Somewhen short before the build process ends it breaks because of an linker error. One way to work around that is to copy qt/lib/QtWebKit4.lib to qt/lib/QtWebKit.lib - yes thats ugly.
* The install_windows.sh script partially fails because of permission denied errors. My way to work around this is to boot into Linux, adjust the cygwin parts of the paths in the auto-generated relocate.sh script and then run it on Linux, and also package there. Fixes appreciated.
