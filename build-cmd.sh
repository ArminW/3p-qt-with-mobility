#!/bin/bash

build_linux(){
        export MAKEFLAGS="-j4"

        # TODO: find out how much of this is actually used
        export  CXXFLAGS="-DQT_NO_INOTIFY -m$1 -fno-stack-protector -O2 -pipe  -fomit-frame-pointer -msse -mfpmath=sse  -msse2 -ftree-vectorize"
        export  CFLAGS="-m$1 -fno-stack-protector -O2 -pipe  -fomit-frame-pointer -msse -mfpmath=sse  -msse2 -ftree-vectorize"
        export LDFLAGS="-m$1"

        pushd "$QT_SOURCE_DIR"
            export QTDIR="$(pwd)"
            export PATH="$QTDIR/bin:$PATH"
            echo "DISTCC_HOSTS=$DISTCC_HOSTS"

            if [ -f $top/$QT_SOURCE_DIR/bin/qmake ]; then
                 make confclean
                 # if make confclean fails try
                 # ./configure -opensource -fast -nomake examples -nomake demos -nomake docs -nomake translations -nomake tools
                 # to be able to make confclean
                 export QMAKESPEC="linux-g++-$1"
            fi

            OPENSSL_LIBS='-L/${packages}/lib/release -lssl -lcrypto' ./configure \
                -v -platform linux-g++-$1 -fast -no-qt3support -release -opensource -confirm-license \
                $(:-static)  -shared -no-rpath\
                -no-3dnow -no-sse3 -no-ssse3 -no-sse4.1 -no-sse4.2 -no-avx -no-neon\
                -no-pch  $(: precompiled headers) \
                -nomake examples -nomake demos -nomake docs \
                $TEAPOTKEY  \
                -no-glib -no-multimedia -no-phonon -no-phonon-backend -webkit -openssl \
                -no-audio-backend\
                -no-declarative \
                -no-cups -qt-sql-sqlite -plugin-sql-sqlite\
                -no-xmlpatterns \
                -fontconfig -xrender -svg \
                -no-sm $(: x11 session management) \
                -no-gtkstyle \
                -no-scripttools \
                -opengl desktop -qt-libmng  $(:-qt-libtiff) -qt-libpng -qt-zlib\
                 -system-libjpeg \
                -I"$packages/include"  \
                -I"$packages/include/fontconfig"  \
                -I"$packages/include/freetype"  \
                -I"$packages/include/gio-unix-2.0"  \
                -I"$packages/include/glib-2.0"  \
                -I"$packages/include/gstreamer-0.10"  \
                -I"$packages/include/jpeglib"  \
                -I"$packages/include/libpng15"  \
                -I"$packages/include/libxml2"  \
                -I"$packages/include/openssl"  \
                -I"$packages/include/unicode"  \
                -I"$packages/include/zlib"  \
                -L"$packages/lib/release" \
                --prefix="$install"    \
                --libdir="$install/lib/release"

#exit 0
            make $MAKEFLAGS
            make install
        popd
        pushd "qt-mobility"
          if [ -f Makefile ];then 
            make clean
          fi
          export QTDIR="$install"
          export PATH="$QTDIR/bin:$PATH"
          export QMKSPEC="INCLUDEPATH+='$stage/packages/include/fontconfig INCLUDEPATH+='$stage/packages/include/freetype INCLUDEPATH+='$stage/packages/include/gio-unix-2.0 INCLUDEPATH+='$stage/packages/include/glib-2.0 INCLUDEPATH+='$stage/packages/include/gstreamer-0.10 INCLUDEPATH+='$stage/packages/include/jpeglib INCLUDEPATH+='$stage/packages/include/libpng15 INCLUDEPATH+='$stage/packages/include/libxml2 INCLUDEPATH+='$stage/packages/include/openssl 
          INCLUDEPATH+='$stage/packages/include/unicode INCLUDEPATH+='$stage/packages/include/zlib' LIBS+=-L'$stage/packages/lib/release'"
          ./configure -release -modules "multimedia" -prefix "$install" -libdir "$install/lib/release"


          make $MAKEFLAGS
          make install
        popd

        # play it again, Sam
        pushd "$QT_SOURCE_DIR"
            OPENSSL_LIBS='-L/${packages}/lib/release -lssl -lcrypto' ./configure \
                -v -platform linux-g++-$1 -fast -no-qt3support -release -opensource -confirm-license \
                $(:-static)  -shared -no-rpath\
                -no-3dnow -no-sse3 -no-ssse3 -no-sse4.1 -no-sse4.2 -no-avx -no-neon\
                -no-pch  $(: precompiled headers) \
                -nomake examples -nomake demos -nomake docs \
                $(:-nomake translations -nomake tools)\
                $TEAPOTKEY  \
                -glib -no-multimedia -no-phonon -no-phonon-backend -webkit -openssl \
                -no-audio-backend\
                -no-declarative \
                -no-cups -qt-sql-sqlite -plugin-sql-sqlite\
                -no-xmlpatterns \
                -fontconfig -xrender -svg \
                -no-sm $(: x11 session management) \
                $(:-no-xinput -no-xkb) \
                -no-gtkstyle \
                -no-scripttools \
                -opengl desktop -qt-libmng  $(:-qt-libtiff) -qt-libpng -qt-zlib\
                 -system-libjpeg \
                -D "ENABLE_WEBGL=1" \
                -I"$packages/include"  \
                -I"$packages/include/fontconfig"  \
                -I"$packages/include/freetype"  \
                -I"$packages/include/gio-unix-2.0"  \
                -I"$packages/include/glib-2.0"  \
                -I"$packages/include/gstreamer-0.10"  \
                -I"$packages/include/jpeglib"  \
                -I"$packages/include/libpng15"  \
                -I"$packages/include/libxml2"  \
                -I"$packages/include/openssl"  \
                -I"$packages/include/unicode"  \
                -I"$packages/include/zlib"  \
                -L"$packages/lib/release" \
                --prefix="$install"    \
                --libdir="$install/lib/release"

#exit 0
            make $MAKEFLAGS
            make install
        popd

        # Voodoo \o/
        pushd "$install/lib/release/pkgconfig"
          for v in $packages $install; do
             find . -type f  -name '*.pc' \
             -exec perl -pi -e "\$baz=quotemeta('$v');s/\$baz/\\$\{prefix\}/g" {} \;
          done
          find . -type f  -name '*.pc' \
             -exec perl -pi -e 's/^prefix\=\$\{prefix\}/prefix\=\$\{PREBUILD_DIR\}/' {} \;
        popd
}

# make errors fatal
set -e

QT_SOURCE_DIR="qt"

if [ -z "$AUTOBUILD" ] ; then 
    fail
fi

if [ "$OSTYPE" = "cygwin" ] ; then
    export AUTOBUILD="$(cygpath -u $AUTOBUILD)"
fi

# load autbuild provided shell functions and variables
eval "$("$AUTOBUILD" source_environment)"
eval "$AUTOBUILD install --skip-license-check"
# turn on verbose debugging output for logging.
set -x

top="$(dirname "$0")"
stage="$(pwd)"
cd "$top"

packages="$stage/packages"

install="$stage/qt"
mkdir -p "$install"

if [ ! -n $TEAPOTKEY ] ;then
   export TEAPOTKEY="-buildkey TEAPOT$(date +%s)"
fi

case "$AUTOBUILD_PLATFORM" in
    "windows")
        load_vsvars
        pushd "$QT_SOURCE_DIR"
        export QTDIR="$(pwd)"
        export PATH="$QTDIR/bin:$PATH"
        export QMAKESPEC="win32-msvc2010"  

        if [ ! -x "configure.exe" ];then
          chmod +x "configure.exe"
        fi
        # this doesn't work on windows
        ## if [ -f bin/qmake.exe ]; then
          # # nmake confclean
        ## fi
        # brute force clean: cd into the qt directory delete anything *but the .git folder* and git reset --hard 

            "./configure.exe" -opensource -confirm-license -fast -mp -release \
            -no-qt3support -no-3dnow   -no-plugin-manifests -nomake demos -nomake examples \
            -no-phonon -no-phonon-backend  -no-audio-backend -no-multimedia -no-webkit \
            -opengl desktop \
            -qt-libjpeg -qt-libpng -openssl \
            -qt-sql-sqlite -plugin-sql-sqlite\
            $TEAPOTKEY  \
            -no-exceptions \
            -no-dbus \
            -I "$(cygpath -m "$packages/include")" \
            -I "$(cygpath -m "$packages/include/jpeglib")" \
            -I "$(cygpath -m "$packages/include/libpng15")" \
            -I "$(cygpath -m "$packages/include/openssl")" \
            -I "$(cygpath -m "$packages/include/zlib")" \
            -L "$(cygpath -m "$packages/lib/release")"

        #nmake

        jom -j 4 #ftp://ftp.qt.nokia.com/jom/ nmake replacement, install it in your $PATH

        popd

        pushd "qt-mobility"
           if [ -f Makefile ];then
             nmake clean
           fi
          export INCLUDE_BASE="$(cygpath -m $packages/include)"
          export INCLUDE_JPEG="$(cygpath -m $packages/include/jpeglib)"
          export INCLUDE_PNG="$(cygpath -m $packages/include/libpng15)"
          export INCLUDE_OPENSSL="$(cygpath -m $packages/include/openssl)"
          export INCLUDE_ZLIB="$(cygpath -m $packages/include/zlib)"
          export LIBS_BASE="$(cygpath -m $packages/lib/release)"
          export QMAKE_OPTIONS="INCLUDEPATH+=$INCLUDE_BASE INCLUDEPATH+=$INCLUDE_JPEG INCLUDEPATH+=$INCLUDE_PNG INCLUDEPATH+=$INCLUDE_OPENSSL INCLUDEPATH+=$INCLUDE_ZLIB LIBS+=-L$LIBS_BASE"

          ./configure.bat -release -modules "multimedia" -prefix "$(cygpath -m $QTDIR)"

          nmake
          nmake install
        popd

        # play it again, Sam
        pushd "$QT_SOURCE_DIR"
            "./configure.exe" -opensource -confirm-license -fast -mp -release \
            -no-qt3support -no-3dnow -no-plugin-manifests -nomake demos -nomake examples \
            -no-phonon -no-phonon-backend  -no-audio-backend -no-multimedia\
            -webkit \
            -opengl desktop \
            -qt-libjpeg -qt-libpng -openssl \
            -qt-sql-sqlite -plugin-sql-sqlite\
            $TEAPOTKEY  \
            -no-exceptions \
            -no-dbus \
            -D "ENABLE_WEBGL=1" \
            -I "$(cygpath -m "$packages/include")" \
            -I "$(cygpath -m "$packages/include/jpeglib")" \
            -I "$(cygpath -m "$packages/include/libpng15")" \
            -I "$(cygpath -m "$packages/include/openssl")" \
            -I "$(cygpath -m "$packages/include/zlib")" \
            -L "$(cygpath -m "$packages/lib/release")"

            # UGLY HACK: 
            #  if you run into a linker error about QtWebKit.lib not found (seems to happen randomly)
            ##  cp -f "$QTDIR/lib/QtWebKit4.lib" "$QTDIR/lib/QtWebKit.lib" 
            ##  cd to  $QTDIR and re-run jom / nmake by hand

            jom -j 4 

        popd

        # Move around libraries to match autobuild layout.
        # if you get permission denied errors boot into Linux and run it there 
        # better solution very welcome
        ./install_windows.sh $QT_SOURCE_DIR $install


    ;;
    "darwin")
        pushd "$QT_SOURCE_DIR"
            export QTDIR="$(pwd)"
            echo "yes" | \
                ./configure -opensource -platform macx-g++40 -no-framework -fast -no-qt3support -prefix "$install" \
                    -static -release -no-xmlpatterns -no-phonon -webkit $TEAPOTKEY -sdk /Developer/SDKs/MacOSX10.5.sdk/ -cocoa \
                    -nomake examples -nomake demos -nomake docs -nomake translations -nomake tools -nomake examples
            make -j4
            make -j4 -C "src/3rdparty/webkit/JavaScriptCore"
            export PATH="$PATH:$QTDIR/bin"
            make install
            
            cp "src/3rdparty/webkit/JavaScriptCore/release/libjscore.a" "$install/lib"
        popd
    ;;
    "linux")
          build_linux 32
    ;;
    "linux64")
          build_linux 64
    ;;
esac

cat > $install/bin/qt.conf.in <<EOF 
[Paths]
Prefix = @QTDIR@
Libraries = lib/release
EOF

mkdir -p "$stage/LICENSES"
cp "$QT_SOURCE_DIR/LICENSE.LGPL" "$stage/LICENSES/qt.txt"

README_DIR="$stage/autobuild-bits"
README_FILE="$README_DIR/README-Version-qt-teapot"
mkdir -p $README_DIR
cat .git/config|grep origin |sed  -e "s/url = ssh:\/\/git@/git:\/\//" > $README_FILE
echo "Commit $(git rev-parse HEAD)" >> $README_FILE

pass

