#!/bin/bash
TOP="$(dirname "$0")"
pushd $TOP

if [ -d qt/.git ]; then
  echo "Qt source already installed, updating submodules"
  git submodule update
else
  echo "Downloading Qt sources. Have a break."
  git submodule update --init
fi
popd
